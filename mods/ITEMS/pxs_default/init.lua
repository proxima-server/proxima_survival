--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Proxima Survival Default
]]--

local M = minetest.get_meta

S = minetest.get_translator("pxs_default")

dofile(minetest.get_modpath("pxs_default") .. "/deoxidizer.lua")
dofile(minetest.get_modpath("pxs_default") .. "/specialitems.lua")
dofile(minetest.get_modpath("pxs_default") .. "/blastfurnace.lua")
dofile(minetest.get_modpath("pxs_default") .. "/alias.lua")

-- General Nodes

minetest.register_node("pxs_default:mars_stone", {
  description = S("Mars Stone"),
  tiles = {"default_mars_stone.png"},
  groups = {picky = 3, stone = 1, oxidized_stone = 1},
  drop = "pxs_default:mars_stone",
})

minetest.register_node("pxs_default:proxima_stone", {
  description = S("Proxima Stone"),
  tiles = {"default_proxima_stone.png"},
  groups = {picky = 4, stone = 1, oxidized_stone = 1},
  drop = "pxs_default:proxima_stone",
})

minetest.register_node("pxs_default:proxima_sand", {
  description = S("Proxima Sand"),
  tiles = {"default_proxima_sand.png"},
  groups = {spady = 1, sand = 1, falling_node = 1},
  drop = "pxs_default:proxima_sand",
})

minetest.register_node("pxs_default:tiny_proxima_stone", {
  description = S("Tiny Proxima Stone"),
  tiles = {"default_proxima_stone.png"},
  groups = {picky = 2, handy = 3, oddly_break_by_hand = 3, tiny_stone = 1, tiny_oxidized_stone = 1},
  drop = "pxs_default:tiny_proxima_stone",
  drawtype = "nodebox",
  node_box = {
	type = "fixed",
	fixed = {
		{-0.2500, -0.5000, -0.1563, 0.2500, -0.3750, 0.1563}
	 }
 },
 is_ground_content = true,
 legacy_mineral = true,
})

minetest.register_node("pxs_default:proxima_stone_with_coal", {
	description = S("Proxima Coal Ore"),
	tiles = {"default_proxima_stone.png^default_mineral_coal.png"},
	groups = {picky = 4},
	drop = "default:coal_lump",
	--sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("pxs_default:proxima_stone_with_coal_glow", {
	description = S("Proxima Glowing Coal Ore"),
	tiles = {"default_proxima_stone.png^default_mineral_coal_glow.png"},
	groups = {picky = 4},
	drop = "default:coal_lump",
  light_source = 12,
})

minetest.register_node("pxs_default:alienflower", {
  description = S("Alienflower"),
  walkable = false,
  tiles = {"default_alienflower_dark.png"},
  drawtype = "plantlike",
  inventory_image = "default_alienflower_dark.png",
  on_use = minetest.item_eat(3),
  groups = {snappy = 3, oddly_break_by_hand = 2},
  light_source = 9,
	selection_box = {
		type = "fixed",
		fixed = { -2/16, -7/16, -2/16, 2/16, 7/16, 2/16 },
	},
})

minetest.register_node("pxs_default:alienflower_original", {
  description = S("Original Alienflower"),
  walkable = false,
  tiles = {"default_alienflower_original.png"},
  drawtype = "plantlike",
  inventory_image = "default_alienflower_original.png",
  on_use = minetest.item_eat(3),
  groups = {snappy = 3, oddly_break_by_hand = 2},
  light_source = 9,
	selection_box = {
		type = "fixed",
		fixed = { -2/16, -7/16, -2/16, 2/16, 7/16, 2/16 },
	},
})

--General Items

minetest.register_craftitem("pxs_default:proxima_stone_stick", {
  description = S("Stone Stick"),
  inventory_image = "default_stone_stick.png",
  groups = {stick = 1, },
})

--[[
Mineral and Ores
]]--

-- Minerals

minetest.register_craftitem("pxs_default:corundum", {
  description = S("Corundum"),
  inventory_image = "default_corundum.png",
  groups = {corundum = 1, mineral = 1, titanium = 3},
})

minetest.register_craftitem("pxs_default:corundum_blue", {
  description = S("Blue Corundum"),
  inventory_image = "default_corundum_blue.png",
  groups = {corundum = 1, mineral = 1},
})


-- Topas

minetest.register_craftitem("pxs_default:topas", {
  description = S("Topas"),
  inventory_image = "default_topas.png",
  groups = {topas = 1, mineral = 1},
})

minetest.register_craftitem("pxs_default:blue_topas", {
  description = S("Blue Topas"),
  inventory_image = "default_blue_topas.png",
  groups = {topas = 1, mineral = 1},
})

minetest.register_craftitem("pxs_default:red_topas", {
  description = S("Red Topas"),
  inventory_image = "default_red_topas.png",
  groups = {topas = 1, mineral = 1},
})

minetest.register_craftitem("pxs_default:yellow_topas", {
  description = S("Yellow Topas"),
  inventory_image = "default_yellow_topas.png",
  groups = {topas = 1, mineral = 1},
})

minetest.register_craftitem("pxs_default:orange_topas", {
  description = S("Orange Topas"),
  inventory_image = "default_orange_topas.png",
  groups = {topas = 1, mineral = 1},
})
minetest.register_craftitem("pxs_default:gray_topas", {
  description = S("Gray Topas"),
  inventory_image = "default_grey_topas.png",
  groups = {topas = 1, mineral = 1},
})
minetest.register_craftitem("pxs_default:purple_topas", {
  description = S("Purple Topas"),
  inventory_image = "default_purple_topas.png",
  groups = {topas = 1, mineral = 1},
})
minetest.register_craftitem("pxs_default:pink_topas", {
  description = S("Pink Topas"),
  inventory_image = "default_pink_topas.png",
  groups = {topas = 1, mineral = 1},
})
minetest.register_craftitem("pxs_default:brown_topas", {
  description = S("Brown Topas"),
  inventory_image = "default_brown_topas.png",
  groups = {topas = 1, mineral = 1},
})
minetest.register_craftitem("pxs_default:green_topas", {
  description = S("Green Topas"),
  inventory_image = "default_green_topas.png",
  groups = {topas = 1, mineral = 1},
})

-- Ores

minetest.register_craftitem("pxs_default:titanium_lump", {
  description = S("Titanium lump"),
  inventory_image = "default_titanium_lump.png",
  groups = {titanium = 1, mineral = 1},
})


-- Craft

minetest.register_craft({
    type = "shaped",
    output = "pxs_default:proxima_stone_stick 8",
    recipe = {
        {"pxs_default:tiny_proxima_stone",},
        {"pxs_default:tiny_proxima_stone",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "pxs_default:proxima_stone_stick 9",
    recipe = {
        {"pxs_default:proxima_stone",},
    }
})

minetest.register_node("pxs_default:chimney_clay_unfired", {
  description = S("Clay chimney unfired"),
  tiles = {"default_clay.png"},
  groups = {crumbly = 3, spady = 1},
  drop = "default:clay",
  drawtype = "nodebox",
  paramtype = "light",
  node_box = {
	   type = "fixed",
	    fixed = {
		      {-0.4375, -0.5000, -0.2500, -0.3750, 0.5000, 0.2500},
		      {0.3750, -0.5000, -0.2500, 0.4375, 0.5000, 0.2500},
		      {-0.2500, -0.5000, -0.4375, 0.2500, 0.5000, -0.3750},
		      {-0.2500, -0.5000, 0.3750, 0.2500, 0.5000, 0.4375},
		      {-0.3750, -0.5000, -0.3125, -0.3125, 0.5000, -0.2500},
          {-0.3125, -0.5000, -0.3750, -0.2500, 0.5000, -0.3125},
		      {0.2500, -0.5000, -0.3750, 0.3125, 0.5000, -0.3125},
		      {0.3125, -0.5000, -0.3125, 0.3750, 0.5000, -0.2500},
		      {0.2500, -0.5000, 0.3125, 0.3125, 0.5000, 0.3750},
		      {0.3125, -0.5000, 0.2500, 0.3750, 0.5000, 0.3125},
		      {-0.3125, -0.5000, 0.3125, -0.2500, 0.5000, 0.3750},
		      {-0.3750, -0.5000, 0.2500, -0.3125, 0.5000, 0.3125}
	       }
       },

})

minetest.register_node("pxs_default:chimney_clay_fired", {
  description = S("Clay chimney fired"),
  tiles = {"clay.png"},
  groups = {crumbly = 3, spady = 1},
  drop = "default:clay",
  drawtype = "nodebox",
  paramtype = "light",
  node_box = {
	   type = "fixed",
	    fixed = {
		      {-0.4375, -0.5000, -0.2500, -0.3750, 0.5000, 0.2500},
		      {0.3750, -0.5000, -0.2500, 0.4375, 0.5000, 0.2500},
		      {-0.2500, -0.5000, -0.4375, 0.2500, 0.5000, -0.3750},
		      {-0.2500, -0.5000, 0.3750, 0.2500, 0.5000, 0.4375},
		      {-0.3750, -0.5000, -0.3125, -0.3125, 0.5000, -0.2500},
          {-0.3125, -0.5000, -0.3750, -0.2500, 0.5000, -0.3125},
		      {0.2500, -0.5000, -0.3750, 0.3125, 0.5000, -0.3125},
		      {0.3125, -0.5000, -0.3125, 0.3750, 0.5000, -0.2500},
		      {0.2500, -0.5000, 0.3125, 0.3125, 0.5000, 0.3750},
		      {0.3125, -0.5000, 0.2500, 0.3750, 0.5000, 0.3125},
		      {-0.3125, -0.5000, 0.3125, -0.2500, 0.5000, 0.3750},
		      {-0.3750, -0.5000, 0.2500, -0.3125, 0.5000, 0.3125}
	       }
       },
	after_place_node = function(pos)
		local meta = M(pos)
		local inv = meta:get_inventory()
		inv:set_size("a", 1)
		inv:set_size("b", 1)
		inv:set_size("c", 1)
		inv:set_size("d", 1)
		meta:set_string("formspec", default.gui_bg .. default.gui_bg_img .. default.gui_slots .. default.get_hotbar_bg(1, 1))
		minetest.get_node_timer(pos):start(0.5)
	end,

      on_rightclick = function(pos, node, player, itemstack, pointed_thing)
        local name = player:get_player_name()
            minetest.show_formspec(name, "pxs_default:chimney",
              "size[3,6]" ..
              "list[context;a;1,1;1,1;]" ..
              "list[context;b;1,2;1,1;]" ..
              "list[context;c;1,3;1,1;]" ..
              "list[context;d;1,4;1,1;]" )
          end,
    on_punch = function(pos, node, player, pointed_thing)
     	local meta = M(pos)
        local inv = meta:get_inventory()
		inv:add_item("a", player:get_wielded_item())
		--TODO
    end,
	allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
		return 0
	end,
    allow_metadata_inventory_put = function(pos, listname, index, stack, player)
		return 0
	end,
    allow_metadata_inventory_take = function(pos, listname, index, stack, player)
		if listname=="d" and minetest.get_node({x=pos.x, y=pos.y-1, z=pos.z})=="air" then return stack:get_count()
		else return 0
		end
	end,
	on_timer = function(pos, elapsed)
		minetest.get_node_timer(pos):start(0.5)
		--TODO
		local meta = M(pos)
		local inv = meta:get_inventory()
		--[[if not inv:is_empty("d")
			inv:add_item("c", )
		end]]
	end,
})


--[[minetest.register_craft({
    type = "shaped",
    output = "pxs_default:proxima_stone",
    recipe = {
        {"pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick",},
        {"pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick",},
        {"pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick", "pxs_default:proxima_stone_stick",},
    }
})]]
