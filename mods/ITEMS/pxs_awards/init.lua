--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Proxima Survival Awards
]]--

minetest.register_node("pxs_awards:cup_half_year", {
  description = "Half Year Cup",
  tiles = {
        "default_cup_half_year_top_down.png",    -- y+
        "default_cup_half_year_top_down.png",  -- y-
        "default_cup_half_year_side.png", -- x+
        "default_cup_half_year_side.png",  -- x-
        "default_cup_half_year_side.png",  -- z+
        "default_cup_half_year_side.png", -- z-
    },
  groups = {picky = 2, handy = 3, oddly_break_by_hand = 3},
  drop = "pxs_awards:cup_half_year",
  drawtype = "nodebox",
  node_box = {
	type = "fixed",
	fixed = {
		{-0.06250, -0.5000, -0.06250, 0.06250, 0.2500, 0.06250},
		{-0.3125, -0.5000, -0.3125, 0.3125, -0.3750, 0.3125},
		{-0.1875, 0.1875, -0.1875, 0.1875, 0.2500, 0.1875},
		{-0.2500, 0.1875, -0.2500, -0.1875, 0.6875, 0.2500},
		{0.1875, 0.1875, -0.2500, 0.2500, 0.6875, 0.2500},
		{-0.2500, 0.1875, 0.1875, 0.2500, 0.6875, 0.2500},
		{-0.2500, 0.1875, -0.2500, 0.2500, 0.6875, -0.1875}
	  }
  },
 is_ground_content = true,
 legacy_mineral = true,
})
