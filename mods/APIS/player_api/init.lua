-- player/init.lua

dofile(minetest.get_modpath("player_api") .. "/api.lua")

-- Default player appearance
player_api.register_model("character.b3d", {
	animation_speed = 30,
	textures = {"character.png", },
	animations = {
		-- Standard animations.
		stand     = {x = 0,   y = 79},
		lay       = {x = 162, y = 166},
		walk      = {x = 168, y = 187},
		mine      = {x = 189, y = 198},
		walk_mine = {x = 200, y = 219},
		sit       = {x = 81,  y = 160},
	},
	collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
	stepheight = 0.6,
	eye_height = 1.47,
})

-- Update appearance when the player joins
minetest.register_on_joinplayer(function(player)
	player_api.player_attached[player:get_player_name()] = false
	player_api.set_model(player, "character.b3d")
	player:set_local_animation(
		{x = 0,   y = 79},
		{x = 168, y = 187},
		{x = 189, y = 198},
		{x = 200, y = 219},
		30
	)
end)

if minetest.settings:get_bool("pxs_multiple_planets", true) then
	minetest.register_on_respawnplayer(function(player)
		local table = #minetest.find_nodes_in_area_under_air({x=0, y=93000, z=0}, {x=0, y=62000, z=0}, {"pxs_default:proxima_sand", "pxs_default:proxima_stone"})
		print(table)
		--player:set_pos({x = 0, y = minetest.find_nodes_in_area_under_air({x=0, y=93000, z=0}, {x=0, y=62000, z=0}, {"pxs_default:proxima_sand", "pxs_default:proxima_stone"})[1], z=0})
	end)
end
