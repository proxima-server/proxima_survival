--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Proxima Survival Tools
]]--

--[[minetest.register_item(":", {
	type = "none",
	wield_image = "wieldhand.png",
	wield_scale = {x=1,y=1,z=2.5},
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level = 0,
		groupcaps = {
			axey = {times={[1]=1.70,[2]=2.50,[3]=3.80}, uses=0, maxlevel=1},
			spady = {times={[1]=0.80,[2]=1.50,[3]=2.00}, uses=0, maxlevel=1},
			snappy = {times={[1]=0.20,[2]=0.50,[3]=1.70}, uses=0, maxlevel=1},
			handy = {times={[1]=0.20,[2]=0.50,[3]=1.70}, uses=0},
		},
		damage_groups = {fleshy=1},
	}
})]]

minetest.override_item("", {
	wield_scale = {x=1,y=1,z=2.5},
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level = 0,
		groupcaps = {
			crumbly = {times={[2]=3.00, [3]=0.70}, uses=0, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=0, maxlevel=1},
			oddly_breakable_by_hand = {times={[1]=3.50,[2]=2.00,[3]=0.70}, uses=0},
			axey = {times={[1]=1.70,[2]=2.50,[3]=3.80}, uses=0, maxlevel=1},
			spady = {times={[1]=0.80,[2]=1.50,[3]=2.00}, uses=0, maxlevel=1},
			handy = {times={[1]=0.20,[2]=0.50,[3]=1.70}, uses=0},
		},
		damage_groups = {fleshy=1},
	}
})

minetest.register_tool("pxs_tools:pick_diamond", {
	description = "Diamond Pickaxe",
	inventory_image = "default_pick_diamond.png",
	tool_capabilities = {
		full_punch_interval = 0.4,
		max_drop_level=3,
		groupcaps={
			picky = {times={[1]=0.10, [2]=0.50, [3]=1.20, [4]=1.50, [5]=1.80, [6]=2.60, [7]=3.20, [8]=3.50, [9]=4.20, [10]=5.50, [11]=20.50}, uses=3000, maxlevel=7},
		},
		damage_groups = {fleshy=5},
	},
	groups = {pickaxe = 1}
})

minetest.register_tool("pxs_tools:pick_stone_oxid", {
	description = "Oxidized Stone Pickaxe",
	inventory_image = "default_pick_stone_oxid.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[2]=3.20, [3]=1.60}, uses=15, maxlevel=1},
			picky = {times={[1]=0.50, [2]=1.20, [3]=1.60, [4]=3.20}, uses=15, maxlevel=1},
		},
		damage_groups = {fleshy=5},
	},
	groups = {pickaxe = 1}
})

minetest.register_tool("pxs_tools:hammer_stone", {
	description = "Stonehammer",
	inventory_image = "default_tool_hammer_stone.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=3,
		--[[groupcaps={
      cracky = {times={[2]=3.20, [3]=1.60}, uses=15, maxlevel=1},
			picky = {times={[1]=0.50, [2]=1.20, [3]=1.60, [4]=3.20}, uses=15, maxlevel=1},
		},]]
		damage_groups = {fleshy=3, sharpnes=3},
	},
	groups = {pickaxe = 1}
})

minetest.register_tool("pxs_tools:hammer_steel", {
	description = "Steelhammer",
	inventory_image = "default_tool_hammer_stone.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=3,
		--[[groupcaps={
      cracky = {times={[2]=3.20, [3]=1.60}, uses=15, maxlevel=1},
			picky = {times={[1]=0.50, [2]=1.20, [3]=1.60, [4]=3.20}, uses=15, maxlevel=1},
		},]]
		damage_groups = {fleshy=3, sharpnes=3},
	},
	groups = {pickaxe = 1}
})

minetest.register_tool("pxs_tools:plier_stone", {
	description = "Stonehammer",
	inventory_image = "default_tool_hammer_stone.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=3,
		--[[groupcaps={
      cracky = {times={[2]=3.20, [3]=1.60}, uses=15, maxlevel=1},
			picky = {times={[1]=0.50, [2]=1.20, [3]=1.60, [4]=3.20}, uses=15, maxlevel=1},
		},]]
		damage_groups = {fleshy=1, sharpnes=1},
	},
	groups = {pickaxe = 1}
})

minetest.register_craft({
	type = "shaped",
	output = "pxs_tools:pick_stone_oxid",
	recipe = {
			{"group:tiny_oxidized_stone",  "group:tiny_oxidized_stone",  "group:tiny_oxidized_stone",},
			{"",  "group:stick",  "",},
			{"",  "group:stick",  "",},
	}
})

minetest.register_craft({
	type = "shaped",
	output = "pxs_tools:hammer_stone",
	recipe = {
			{"group:stone",  "group:stone",  "group:stone",},
			{"group:stone",  "group:stick",  "group:stone",},
			{"",  "group:stick",  "",},
	}
})

minetest.register_craft({
	type = "shaped",
	output = "pxs_tools:hammer_steel",
	recipe = {
			{"default:steel_ingot",  "default:steel_ingot",  "default:steel_ingot",},
			{"default:steel_ingot",  "group:stick",  "default:steel_ingot",},
			{"",  "group:stick",  "",},
	}
})

minetest.register_craft({
	type = "shaped",
	output = "pxs_tools:plier_stone",
	recipe = {
			{"",  "group:stone",  "",},
			{"group:stone",  "group:stick",  "",},
			{"",  "group:stick",  "",},
	}
})

minetest.register_tool("pxs_tools:pick_titanium", {
	description = "Titanium Pickaxe",
	inventory_image = "default_pick_titanium.png",
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=3,
		groupcaps={
			picky = {times={[1]=1.00, [2]=1.50, [3]=2.00, [4]=3.00, [5]=3.50, [6]=7.00}, uses=8192, maxlevel=2},
		},
		damage_groups = {fleshy=5},
	},
	groups = {pickaxe = 1}
})

minetest.register_craft({
	type = "shaped",
	output = "pxs_tools:pick_titanium",
	recipe = {
			{"pxs_default:titanium_lump",  "pxs_default:titanium_lump",  "pxs_default:titanium_lump",},
			{"",  "group:stick",  "",},
			{"",  "group:stick",  "",},
	}
})

function register_topas_pxs_tools(def)
	minetest.register_tool("pxs_tools:" .. def.pickname, {
		description = def.pickdescription,
		inventory_image = def.pickimage,
		tool_capabilities = {
			full_punch_interval = 0.5,
			max_drop_level=3,
			groupcaps={
				picky = {times={[1]=0.30, [2]=0.90, [3]=1.50, [4]=1.90, [5]=2.80, [6]=3.30, [7]=3.90, [8]=4.50, [9]=17.20}, uses=1000, maxlevel=7},
			},
			damage_groups = {fleshy=5},
		},
		groups = {pickaxe = 1, topas_pick = 1}
	})

	minetest.register_craft({
	    type = "shaped",
	    output = "pxs_tools:" .. def.pickname,
	    recipe = {
	        {def.topasname,  def.topasname,  def.topasname,},
					{"",  "group:stick",  "",},
					{"",  "group:stick",  "",},
	    }
	})
end

register_topas_pxs_tools({
	pickname = "pick_topas",
	pickdescription = "Topas Pickaxe",
	pickimage = "default_pick_topas.png",
	topasname = "pxs_default:topas",
})

register_topas_pxs_tools({
	pickname = "pick_topas_blue",
	pickdescription = "Topas Pickaxe Blue",
	pickimage = "default_pick_topas_blue.png",
	topasname = "pxs_default:blue_topas",
})

register_topas_pxs_tools({
	pickname = "pick_topas_red",
	pickdescription = "Topas Pickaxe Red",
	pickimage = "default_pick_topas_red.png",
	topasname = "pxs_default:red_topas",
})

register_topas_pxs_tools({
	pickname = "pick_topas_yellow",
	pickdescription = "Topas Pickaxe Yellow",
	pickimage = "default_pick_topas_yellow.png",
	topasname = "pxs_default:yellow_topas",
})
