--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Biomes
]]--

local multiple_planets = minetest.settings:get_bool("pxs_multiple_planets")

minetest.register_alias("mapgen_stone", "pxs_default:proxima_stone")
minetest.register_alias("mapgen_water_source", "pxs_default:mars_stone")


if not multiple_planets then
minetest.register_biome({
  name = "Stoneland",
  node_top = "pxs_default:proxima_sand",
  depth_top = 1,
  node_filler = "pxs_default:proxima_stone",
  depth_filler = 3,
  node_stone = "pxs_default:proxima_stone",
  node_riverbed = "pxs_default:mars_stone",
  depth_riverbed = 2,
  node_dungeon = "pxs_default:mars_stone",
  node_dungeon_stair = "pxs_default:mars_stone",
  y_max = 31000,
  y_min = -31000,
  heat_point = 92,
  humidity_point = 16,
})

minetest.register_decoration({
    name = "pxs_default:alienflower",
		deco_type = "simple",
		place_on = {"pxs_default:proxima_sand"},
    sidelen = 16,
		fill_ratio = 0.0001,
		biomes = {"Stoneland"},
		y_max = 31000,
		y_min = 0,
		decoration = "pxs_default:alienflower",
    spawn_by = "air",
	})

minetest.register_decoration({
    name = "pxs_default:tiny_proxima_stone",
    deco_type = "simple",
    place_on = {"pxs_default:proxima_sand"},
    sidelen = 4,
    fill_ratio = 0.005,
    biomes = {"Stoneland"},
    y_max = 31000,
    y_min = 0,
    decoration = "pxs_default:tiny_proxima_stone",
})
elseif multiple_planets then
  minetest.register_biome({
    name = "Stoneland",
    node_top = "pxs_default:proxima_sand",
    depth_top = 1,
    node_filler = "pxs_default:proxima_stone",
    depth_filler = 3,
    node_stone = "pxs_default:proxima_stone",
    node_riverbed = "pxs_default:mars_stone",
    depth_riverbed = 2,
    node_dungeon = "pxs_default:mars_stone",
    node_dungeon_stair = "pxs_default:mars_stone",
    y_max = 93000,
    y_min = 31000,
    heat_point = 92,
    humidity_point = 16,
  })

  minetest.register_decoration({
      name = "pxs_default:alienflower",
  		deco_type = "simple",
  		place_on = {"pxs_default:proxima_sand"},
      sidelen = 16,
  		fill_ratio = 0.0001,
  		biomes = {"Stoneland"},
  		y_max = 93000,
  		y_min = 62000,
  		decoration = "pxs_default:alienflower",
      spawn_by = "air",
  	})

  minetest.register_decoration({
      name = "pxs_default:tiny_proxima_stone",
      deco_type = "simple",
      place_on = {"pxs_default:proxima_sand"},
      sidelen = 4,
      fill_ratio = 0.005,
      biomes = {"Stoneland"},
      y_max = 93000,
      y_min = 62000,
      decoration = "pxs_default:tiny_proxima_stone",
  })
end
