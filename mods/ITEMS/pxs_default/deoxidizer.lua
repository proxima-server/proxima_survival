--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Deoxidizer
]]--

deoxidizer = {}
deoxidizer.recipes = {}

--Recipes

i3.register_craft_type("deoxidize", {
	description = "Deoxidize",
	icon = "pxs_default_deoxidizer_1.png",
})

function deoxidizer.register_recipe(input, output_iron, output_trash)
	table.insert(deoxidizer.recipes, {input = input, oi = output_iron, ot = output_trash})
	i3.register_craft {
		type   = "deoxidize",
		result = output_iron,
		items  = {input},
	}
end

local function deoxidizer_do(pos)
	local inv = minetest.get_meta(pos):get_inventory()
	local input = inv:get_stack("input", 1)
	local output_iron = inv:get_stack("output_iron", 1)
	local output_trash = inv:get_stack("output_trash", 1)
	for i, v in pairs(deoxidizer.recipes) do
		if inv:contains_item("input", v.input) then
			inv:add_item("output_iron", v.oi)
			inv:remove_item("input", v.input)
		end
	end
end

minetest.register_node("pxs_default:deoxidizer_1", {
	tiles = {"pxs_default_deoxidizer_1.png",},
	description = "Deoxidizer Level 1",
	groups = {picky = 4,},
	after_place_node = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		meta:set_string("infotext", "Deoxidizer")
		inv:set_size("fuel", 1)
		inv:set_size("input", 1)
		inv:set_size("output_iron", 1)
    	inv:set_size("output_trash", 1)
		meta:set_string("formspec", [[
			size[8,8]
			box[-0.01,0;1.84,0.9;#555555]
			image[0,0;0.9,0.9;default_iron_lump.png]
			label[0.65,0.25;Deoxidizer]
			list[context;input;5,0;1,1;]
			list[context;fuel;2,1;1,1;]
			image[3,1;1,1;default_furnace_fire_bg.png]
			image[5,1;1,1;gui_furnace_arrow_bg.png^[transformR180]
			list[context;output_iron;5,2;1,1;]
			list[current_player;main;0,4;8,4;]
		]])
	end,
	on_metadata_inventory_put = function(pos)
		deoxidizer_do(pos)
	end,
	on_metadata_inventory_take = function(pos)
		deoxidizer_do(pos)
	end,
	on_metadata_inventory_move = function(pos)
		deoxidizer_do(pos)
	end,
	allow_metadata_inventory_put = function(pos, listname, index, stack, player)
		if listname == "output_iron" then
			return 0
		else
			return stack:get_count()
		end
	end,
})

deoxidizer.register_recipe("pxs_default:red_topas", "pxs_default:blue_topas")
