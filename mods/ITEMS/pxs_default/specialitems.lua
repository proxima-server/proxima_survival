--[[
	This file is part of Proxima Survival, a subgame of minetest about survivaling on Proxima Centauri B.

	Copyright (C) 2021-2024  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	==============
	Specialitems
]]--

minetest.register_tool("pxs_default:op_posometer", {
  description = "Posometer -- Use to get printed your pos",
  inventory_image = "default_posometer.png",
  groups = {},
  on_use = function(itemstack, player, pointed_thing)
    local pos = player:get_pos()
    local wear = itemstack:get_wear()
    minetest.chat_send_player(player:get_player_name(), "x: " .. pos.x .. " y: " .. pos.y .. " z: " .. pos.z)
  end,
  stack_max = 1,
})
