# Experimental features
# Test features which are in testing phase
# Not working right!
pxs_experminetal (Experimental features) bool false

# More planets then proxima centauri b
# This setting is experimental and require experimental features activated and require minetest 32
pxs_multiple_planets (More planets then proxima centauri b) bool false

# Choose on which World/Planet you want spawn
# This setting can only used if multiple planets is activated.
# Else you spawn on proxima centauri by default
pxs_spawn_world (Spawn World) enum Proxima_Centauri_B Proxima_Centauri_B,Earth

# Choose how to spawn.
#
#- in_area_under_air: use minetest.find_nodes_in_area_under_air({x,world0,z},
#                                {x,world0+2000,z}). Sad is you can spawn in caves
#- regenerate(recomend): generate the map new using mgutils. Only works for v6,v7 and valleys.
#              But v6 work for other reasons not. Experimental!
#- upglitching: Your spawnpoint ist x,world0,z. If there is allready a node y will set one
#               higher. If there is only one node air it also set one higher. If two nodes
#               or more air there is you spawn. This loop again and end if 3rd case entry.
#               Sad is you can spawn in caves.
#
# This setting require experimental feature and work not to time.
pxs_spawn_type (Spawn Type) enum in_area_under_air in_area_under_air,regenerate,upglitching
