## Main Content
Survive on Proxima Centauri B!

Dependencies:
You need minetest Egine

License:
Copyright (C) 2021-2024 debiankaios
Code: Licensed under the GNU GPL version 3 or later. See LICENSE.txt
      exception: /mods/MISC/electronic
Textures and Sounds: CC BY-SA 3.0
The textures are by Bambusmann, debiankaios and JoSto.


Mods which are not from me:

- awards [/mods/APIS/awards]
- bucket [/mods/ITEMS/bucket]
- climate_api [/mods/APIS/climate_api]
- default [/mods/ITEMS/default]
- i3 [/mods/MISC/i3]
- mobs [/mods/OBJECTS/mobs]
- networks [/mods/APIS/networks]
- online_players [/mods/MISC/online_players]
- player_api [/mods/APIS/player_api]
- screwdirver [/mods/ITEMS/screwdirver]
- signs_lib [/mods/APIS/signs_lib]
- stamina [/mods/MISC/stamina]
- stats [/mods/MISC/stats]
- tubelib2 [/mods/APIS/tubelib2]
- Ziped Packages under /mods/*


Some mods have other license, please show byself

Remarks:
This subgame is in alpha-status, Please understand, that it isn't finish.

## This Repository

### Install this repo
```sh
$ git clone https://gitlab.com/debiankaios/proxima_survival
$ git submodule init
```

### Update this repo
```sh
$ git pull
$ git submodule updates
```
